#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='surf17',
    version='0.1.2',
    description=(
        'Simulation of quantum circuits under somewhat realistic condititons'
    ),
    packages=find_packages('.'),
    ext_package='surf17',
    install_requires=[
        "pytools",
        "numpy>=1.12",
        "pytest",
    ]
)
