import argparse
import errno
import numpy as np
import os
import xarray as xr

from time import strftime, localtime
from surf17 import Surface17


def parse_args():
    parser = argparse.ArgumentParser(
        description='Surface17 quantumsim simulator and adaptive weight '
                    'decoder script')
    parser.add_argument('experiment_name', type=str,
                        help='The name of the experiment to run')
    parser.add_argument('--output-dir', '-o', type=str, required=False,
                        help='Output directory', default=os.getcwd())
    parser.add_argument('--seed-start', '-s', type=int, required=False,
                        help='Start of a seed chunk', default=0)
    parser.add_argument('--seed-end', '-S', type=int, required=False,
                        help='End of a seed chunk', default=20000)
    parser.add_argument('--num-cycles', '-n', type=int, required=False,
                        help='Number of cycles per run', default=50)
    parser.add_argument('--batch-size', '-b', type=int, required=False,
                        help='Size of seed batch in one run', default=None)
    return parser.parse_args()


def run_batch(experiment_name, folder, setup, num_cycles, seed_list,
              use_log_superpos=False, surface_class=None):
    try:
        os.makedirs(folder)
    except OSError as err:
        if err.errno != errno.EEXIST:
            raise

    surface_class = surface_class or Surface17

    # this is only a dummy seed and the circuit rng is re-seeded
    # each experiment rep
    surf17_circuit = surface_class(seed=42, setup=setup)
    surf17_circuit.set_compiler(num_cycles=num_cycles,
                                use_log_superpos=use_log_superpos)
    results = surf17_circuit.run_batch(seed_list)
    out = xr.merge([setup, results])
    filename = strftime(
        '%Y-%m-%d_%H:%M_{}_seed{}_{}_ncycles{}.nc'
        .format(experiment_name, seed_list[0], seed_list[-1], num_cycles),
        localtime())
    out.to_netcdf(os.path.join(folder, filename))


def run(setup, use_log_superpos=False, name_suffix=None, surface_class=None):
    args = parse_args()

    # compute the job from SLURM environment, if available
    task_id = os.getenv('SLURM_ARRAY_TASK_ID')
    if task_id is not None:
        task_id = int(task_id)
        task_min = int(os.getenv('SLURM_ARRAY_TASK_MIN'))
        task_max = int(os.getenv('SLURM_ARRAY_TASK_MAX'))
        ntasks = task_max - task_min + 1
        seed_list = np.array_split(np.arange(
            args.seed_start, args.seed_end), ntasks)[task_id-task_min]
    else:
        seed_list = np.arange(args.seed_start, args.seed_end)

    if args.batch_size is None:
        batches = [seed_list]
    else:
        n_batches = len(seed_list) // args.batch_size
        if n_batches == 0:
            n_batches = 1
        batches = np.array_split(seed_list, n_batches)

    for batch in batches:
        name = args.experiment_name
        if name_suffix is not None:
            name += "_" + name_suffix
        run_batch(experiment_name=name,
                  folder=args.output_dir,
                  setup=setup,
                  num_cycles=args.num_cycles,
                  seed_list=batch,
                  use_log_superpos=use_log_superpos,
                  surface_class=surface_class,
                  )
