"""
Setup: a class to make and store setup files.
Setup files contain the experimental details required by
quantumsim to simulate actual quantum hardware given a
circuit (i.e. as a theorist would define).
"""
import json
import numpy as np
import xarray as xr

from toolz import merge


def load_setup(filename):
    if filename.endswith('.json'):
        with open(filename, 'r') as infile:
            setup_dict = json.load(infile)
        return xr.Dataset.from_dict(setup_dict)
    else:
        try:
            return xr.open_dataset(filename)
        except OSError:
            raise ValueError(
                'Unknown file format, setup must be either NETCDF dataset or '
                'JSON dictionary')


def save_setup(setup, filename):
    """

    Parameters
    ----------
    setup: xarray.Dataset
    filename: str
        Filename to dump into
    """
    if filename.endswith('.json'):
        with open(filename, 'w') as outfile:
            json.dump(setup.to_dict(), outfile)
    elif filename.endswith('.nc'):
        setup.to_netcdf(filename)
    else:
        raise ValueError('Filename must end with ".json" for JSON or ".nc" '
                         'for NETCDF.')


def quick_setup(seed, **kwargs):
    rng = np.random.RandomState(seed)
    t1 = 30000.
    t2_sweetspot = 30000.
    t2_int_data = 7000.
    t2_int_anc = 5500.
    t2_park_lf = 8000.
    t2_park_mf = 7300.
    qubit_dic = merge({
            # Low-frequency qubits
            q: get_qubit(rng=rng,
                         t1=t1,
                         t2=t2_sweetspot,
                         t2_int=np.nan,
                         t2_park=t2_park_lf,
                         cphase_leakage_rate=1e-9,
                         cphase_leakage_mobility_rate=0.,
                         qstatic_flux=0.,
                         cphase_sensitivity=40.,
                         cphase_phase_diff_02_12=0.,
                         cphase_phase_diff_20_21=0.,
                         **kwargs) for q in ['D0', 'D1', 'D2',
                                             'D6', 'D7', 'D8']
        }, {
            # Mid-frequency qubits
            q: get_qubit(rng=rng,
                         t1=t1,
                         # Hack: most of the time ancillas spend in the
                         # parking position, so default t2 is parking one;
                         #
                         t2=t2_sweetspot,
                         t2_int=t2_int_anc,
                         t2_park=t2_park_mf,
                         cphase_leakage_rate=0.0005,
                         cphase_leakage_mobility_rate=0.,
                         qstatic_flux=0.,
                         cphase_sensitivity=40.,
                         cphase_phase_diff_02_12=0.,
                         cphase_phase_diff_20_21=0.,
                         **kwargs) for q in ['X0', 'X1', 'X2', 'X3',
                                             'Z0', 'Z1', 'Z2', 'Z3']
        }, {
            # High-frequency qubits
            q: get_qubit(rng=rng,
                         t1=t1,
                         t2=t2_sweetspot,
                         t2_int=t2_int_data,
                         t2_park=np.nan,
                         cphase_leakage_rate=0.0003,
                         cphase_leakage_mobility_rate=0.001,
                         qstatic_flux=0.,
                         cphase_sensitivity=40.,
                         cphase_phase_diff_02_12=1.25 * np.pi,
                         cphase_phase_diff_20_21=0.25 * np.pi,
                         **kwargs) for q in ['D3', 'D4', 'D5']
        }
    )
    qubit_list = list(qubit_dic.keys())
    circuit_params = get_circuit_params(**kwargs)
    qubit_params = list(qubit_dic[qubit_list[0]].keys())

    cphase_model = kwargs.pop('cphase_model', 'precomputed')
    if cphase_model == 'blackbox':
        cphase_params = dict(
            cphase_model='blackbox',
            cphase_coherent=kwargs.pop('cphase_coherent', True),
            cphase_rise_time=2.,
            cphase_int_time=30.,
            cphase_phase_corr_time=10.,
            cphase_angle=np.pi,
            cphase_phase_corr_error=0.,
            cphase_leakage_model=kwargs.pop('cphase_leakage_model', 'coherent'),
            cphase_integrate_idling=True,
        )
    elif cphase_model == 'precomputed':
        cphase_params = dict(
            cphase_model=cphase_model,
            cphase_leakage_model=kwargs.pop('cphase_leakage_model', 'coherent'),
            cphase_mid_high='qsim_qm_qh_int_30_corr_10_full_cz_ptm.npy',
            cphase_low_mid='qsim_ql_qm_int_30_corr_10_full_cz_ptm_reduced.npy'
        )
    else:
        raise ValueError('Unknown CPhase model: {}'.format(cphase_model))

    return xr.Dataset(
        coords=merge(
            {'qubit': qubit_list},
            {param: ('qubit', [qubit_dic[qubit][param] for qubit in qubit_list])
             for param in qubit_params},
            circuit_params,
            cphase_params,
        ))


def get_qubit(rng, *,
              noise_flag=True,
              t1=30000.,
              t2=30000.,
              t2_int=18000.,
              t2_park=18000.,
              anharmonicity=305.,
              dephasing_axis=1e-4,
              dephasing_angle=5e-4,
              static_flux_std=None,
              cphase_leakage_rate=0.,
              cphase_leakage_phase=-np.pi*0.5,
              cphase_leakage_mobility_rate=0.,
              cphase_sensitivity=40.,
              cphase_phase_diff_02_12=np.pi,
              cphase_phase_diff_20_21=0.,
              **kwargs):
    """
    The dictionary for parameters of the DiCarlo qubits, with standard
    parameters pre-set.

    This is a bit messy right now, but has the advantage of telling
    the user which parameters they can set. Not sure how to improve
    over this.
    """
    assert rng

    if static_flux_std is not None:
        qstatic_flux = static_flux_std * rng.randn()
    else:
        qstatic_flux = 0.

    if noise_flag is True:
        param_dic = {
            't1': t1,
            't2': t2,
            't2_int': t2_int,
            't2_park': t2_park,
            'anharmonicity': anharmonicity,
            'dephasing_axis': dephasing_axis,
            'dephasing_angle': dephasing_angle,
            'qstatic_flux': qstatic_flux,
            'cphase_leakage_rate': cphase_leakage_rate,
            'cphase_leakage_phase': cphase_leakage_phase,
            'cphase_leakage_mobility_rate': cphase_leakage_mobility_rate,
            'cphase_sensitivity': cphase_sensitivity,
            'cphase_phase_diff_02_12': cphase_phase_diff_02_12,
            'cphase_phase_diff_20_21': cphase_phase_diff_20_21,
        }
    else:
        param_dic = {
            't1': np.inf,
            't2': np.inf,
            't2_int': np.inf,
            't2_park': t2_park,
            'anharmonicity': anharmonicity,
            'dephasing_axis': 0,
            'dephasing_angle': 0,
            'qstatic_flux': 0,
            'cphase_leakage_rate': 1e-9,
            'cphase_leakage_phase': cphase_leakage_phase,
            'cphase_leakage_mobility_rate': 0,
            'cphase_sensitivity': 0,
            'cphase_phase_diff_02_12': 0,
            'cphase_phase_diff_20_21': 0,
        }

    return param_dic


def get_circuit_params(noise_flag=True,
                       p_exc_init=0.0,
                       p_dec_init=0.005,
                       p_exc_fin=0.0,
                       p_dec_fin=0.015,
                       t_gate=20,
                       t_cph_gate=40,
                       t_meas=300,
                       t_readout=300,
                       t_cycle=800,
                       readout_error=0.0015,
                       meas_basis='Z',
                       **kwargs):

    if noise_flag is True:
        param_dic = {
            'p_exc_init': p_exc_init,
            'p_dec_init': p_dec_init,
            'p_exc_fin': p_exc_fin,
            'p_dec_fin': p_dec_fin,
            't_gate': t_gate,
            't_cph_gate': t_cph_gate,
            't_meas': t_meas,
            't_readout': t_readout,
            't_cycle': t_cycle,
            'readout_error': readout_error,
            'meas_basis': meas_basis,
        }
    else:
        param_dic = {
            'p_exc_init': 0,
            'p_dec_init': 0,
            'p_exc_fin': 0,
            'p_dec_fin': 0,
            't_gate': t_gate,
            't_cph_gate': t_cph_gate,
            't_meas': t_meas,
            't_readout': t_readout,
            't_cycle': t_cycle,
            'readout_error': 0,
            'meas_basis': meas_basis,
        }

    return param_dic


def get_bb_cphase_params():
    return


def eval_t2(t_relax, t_dephasing):
    return 2*t_relax*t_dephasing / (t_dephasing + (2*t_relax))
