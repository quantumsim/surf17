import os
from collections import defaultdict
import numpy as np
import xarray as xr

from quantumsim import ptm
from quantumsim.circuit import Circuit, CPhase, FluxPulse, \
    BiasedSampler, AmpPhDamp, RotateY
from quantumsim import dm10_general as dm10g
from surf17.models.gates import idle, cphase, det_pulse


class Surface17:
    def __init__(self, seed, setup):
        self.setup = setup
        self._rng = np.random.RandomState(seed)

        self._data_bits = ["D%d" % i for i in range(9)]
        self._x_anc_bits = ["X%d" % i for i in range(4)]
        self._z_anc_bits = ["Z%d" % i for i in range(4)]
        self._meas_bits = [
            "M" + bit for bit in self._x_anc_bits + self._z_anc_bits]

        fg_red = ["D0", "D2", "D6", "D8"]
        fg_red_p = ["D1", "D7"]
        fg_purp = ["D3", "D5"]
        fg_purp_p = ["D4"]
        fg_blue = ["X1", "X3"]
        fg_blue_p = ["X0", "X2"]
        fg_green = ["Z2", "Z3"]
        fg_green_p = ["Z0", "Z1"]

        self._low_freq_bits = fg_red + fg_red_p
        self._high_freq_bits = fg_purp + fg_purp_p
        self._mid_freq_bits = fg_green + fg_green_p + fg_blue + fg_blue_p

        self.qubit_freq = {}
        self.qubit_anharm = {}
        for bit in self._high_freq_bits:
            self.qubit_freq[bit] = 6.725
            self.qubit_anharm[bit] = self._p('anharmonicity', bit)
        for bit in self._low_freq_bits:
            self.qubit_freq[bit] = 4.925
            self.qubit_anharm[bit] = self._p('anharmonicity', bit)
        for bit in self._mid_freq_bits:
            self.qubit_freq[bit] = 6.025
            self.qubit_anharm[bit] = self._p('anharmonicity', bit)

        self._resonators = [(anc, dat)
                            if self.qubit_freq[anc] < self.qubit_freq[dat]
                            else (dat, anc) for anc, dats in
                            [("X0", ["D2", "D1"]),
                             ("X1", ["D1", "D0", "D4", "D3"]),
                             ("X2", ["D5", "D4", "D8", "D7"]),
                             ("X3", ["D7", "D6", ]),
                             ("Z0", ["D0", "D3", ]),
                             ("Z1", ["D2", "D5", "D1", "D4"]),
                             ("Z2", ["D4", "D7", "D3", "D6"]),
                             ("Z3", ["D5", "D8"])] for dat in dats]

        self._x_stab_sequences = [
            fg_red_p + fg_blue_p + fg_purp_p,
            fg_red + fg_blue_p + fg_purp,
            fg_red + fg_blue + fg_purp,
            fg_red_p + fg_blue + fg_purp_p
        ]

        self._z_stab_sequences = [
            fg_red + fg_green + fg_purp,
            fg_red_p + fg_green_p + fg_purp_p,
            fg_red_p + fg_green + fg_purp_p,
            fg_red + fg_green_p + fg_purp,
        ]

        self._ptm_gates_folder = os.path.join(
            os.path.dirname(__file__), 'models', 'precomputed')
        if setup.cphase_model == 'precomputed':
            self._mid_high_cphase = np.load(os.path.join(
                self._ptm_gates_folder,
                setup.cphase_mid_high.item()
            )).reshape(9, 9, 9, 9)
            self._low_mid_cphase = np.load(os.path.join(
                self._ptm_gates_folder,
                setup.cphase_low_mid.item()
            )).reshape(9, 9, 9, 9)
        if setup.cphase_model == 'precomputed':
             self._detuning_gate = np.load(os.path.join(
                self._ptm_gates_folder, 'detuning_pulse_coherent_ptm.npy'))

        self._op_dephase_leakage = (
            0.5 * ptm.ConjunctionPTM(np.eye(3)) +
            0.5 * ptm.ConjunctionPTM(np.diag([1, 1, -1])))

        self._circuit = None
        self._operators = None
        self._compiler = None

    def _reseed_rng(self, new_seed):
        self._rng = np.random.RandomState(new_seed)

    def _p(self, param, qubit=None):
        if qubit:
            return self.setup.sel(qubit=qubit)[param].item()
        else:
            return self.setup[param].item()

    def _prep_circuit(self, circuit_name="Default Circuit Name"):
        circuit = Circuit(circuit_name)

        for qubit in self._data_bits + self._x_anc_bits + self._z_anc_bits:
            p = self.setup.sel(qubit=qubit)
            circuit.add_qubit(qubit, p.t1.item(), p.t2.item())
        for meas_bit in self._meas_bits:
            circuit.add_qubit(meas_bit)

        return circuit

    def _get_detuning(self, bit, basis):
        if self._p('cphase_model') == 'precomputed':
            return ptm.ExplicitBasisPTM(self._detuning_gate, basis)
        elif self._p('cphase_model') == 'blackbox':
            p = self.setup.sel(qubit=bit)
            return det_pulse(p.t1.item(), p.t2.item(), p.t2_park.item(),
                             self.qubit_anharm[bit],
                             rise_time=p.cphase_rise_time.item(),
                             det_time=p.cphase_int_time.item(),
                             phase_corr_time=p.cphase_phase_corr_time.item())
        else:
            raise ValueError('Unknown CPhase model: {}'
                             .format(self._p('cphase_model')))

    def _get_cphase(self, stat_q, fluxed_q, basis):
        if self._p('cphase_model') == 'precomputed':
            if fluxed_q in self._high_freq_bits:
                cphase_operator = ptm.TwoPTMExplicit(
                    self._mid_high_cphase, (basis, basis))
            elif fluxed_q in self._mid_freq_bits:
                cphase_operator = ptm.TwoPTMExplicit(
                    self._low_mid_cphase, (basis, basis))
            else:
                raise ValueError(
                    "The pulsed qubit should be either be either "
                    "a high or a mid freq one")
        elif self._p('cphase_model') == 'blackbox':
            for t in 't1', 't2':
                for q in stat_q, fluxed_q:
                    assert self._p(t, q) > 0
            cphase_operator = cphase(
                q0_t1=self._p('t1', stat_q),
                q0_t2=self._p('t2', stat_q),
                q1_t1=self._p('t1', fluxed_q),
                q1_t2=self._p('t2', fluxed_q),
                q1_t2_int=self._p('t2_int', fluxed_q),
                q0_anharmonicity=self.qubit_anharm[stat_q],
                q1_anharmonicity=self.qubit_anharm[fluxed_q],
                rise_time=self._p('cphase_rise_time'),
                int_time=self._p('cphase_int_time'),
                phase_corr_time=self._p('cphase_phase_corr_time'),
                angle=self._p('cphase_angle'),
                phase_corr_error=self._p('cphase_phase_corr_error'),
                leakage_rate=self._p('cphase_leakage_rate', fluxed_q),
                leakage_phase=self._p('cphase_leakage_phase', fluxed_q),
                leakage_mobility_rate=self._p('cphase_leakage_mobility_rate',
                                              fluxed_q),
                quasistatic_flux=self._p('qstatic_flux', fluxed_q),
                sensitivity=self._p('cphase_sensitivity', fluxed_q),
                phase_diff_02_12=self._p('cphase_phase_diff_02_12', fluxed_q),
                phase_diff_20_21=self._p('cphase_phase_diff_20_21', fluxed_q),
                integrate_idling=self._p('cphase_integrate_idling'),
            )
        else:
            raise ValueError(
                'Unknown CPhase model: {}'.format(self._p('cphase_model')))

        if self._p('cphase_leakage_model') == 'coherent':
            return cphase_operator
        elif self._p('cphase_leakage_model') == 'incoherent':
            return ptm.TwoPTMProduct(
                [((0, 1), cphase_operator),
                 ((0, ), self._op_dephase_leakage),
                 ((1, ), self._op_dephase_leakage)])
        else:
            raise ValueError('Unknown leakage model: {}'
                             .format(self._p('cphase_leakage_model')))

    @property
    def circuit(self):
        if self._circuit is None:
            circuit = self._prep_circuit(circuit_name="surf_17_cycle")

            t_next_cphase = (self.setup.t_gate.item() +
                             self.setup.t_cph_gate.item()) / 2

            for det_sequence in self._x_stab_sequences:
                active_low_freq_bits = []
                for anc, dat in self._resonators:
                    if anc in self._x_anc_bits or dat in self._x_anc_bits:
                        if anc in det_sequence and dat not in det_sequence:
                            cphase_gate = CPhase(
                                anc, dat, time=t_next_cphase,
                                int_time=self.setup.t_cph_gate.item())
                            circuit.add_gate(cphase_gate)
                            if anc in self._low_freq_bits:
                                active_low_freq_bits.append(anc)
                for low_freq_bit in self._low_freq_bits:
                    if low_freq_bit not in active_low_freq_bits:
                        detuning_pulse = FluxPulse(
                            low_freq_bit,
                            time=t_next_cphase,
                            int_time=self.setup.t_cph_gate.item())
                        circuit.add_gate(detuning_pulse)

                t_next_cphase += self.setup.t_cph_gate.item()

            z_seq_start = self.setup.t_gate.item() + \
                (4 * self.setup.t_cph_gate.item())
            t_next_cphase = z_seq_start + ((self.setup.t_gate.item() +
                                            self.setup.t_cph_gate.item())/2)

            for det_sequence in self._z_stab_sequences:
                active_low_freq_bits = []
                for anc, dat in self._resonators:
                    if anc in self._z_anc_bits or dat in self._z_anc_bits:
                        if anc in det_sequence and dat not in det_sequence:
                            cphase_gate = CPhase(
                                anc, dat, time=t_next_cphase,
                                int_time=self.setup.t_cph_gate.item())
                            circuit.add_gate(cphase_gate)
                            if anc in self._low_freq_bits:
                                active_low_freq_bits.append(anc)
                for low_freq_bit in self._low_freq_bits:
                    if low_freq_bit not in active_low_freq_bits:
                        detuning_pulse = FluxPulse(
                            low_freq_bit, time=t_next_cphase,
                            int_time=self.setup.t_cph_gate.item())
                        circuit.add_gate(detuning_pulse)
                t_next_cphase += self.setup.t_cph_gate.item()

            for dat in self._data_bits:
                p = self.setup.sel(qubit=dat)
                circuit.add_rotate_y(
                    dat, angle=np.pi / 2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=0,
                )
                circuit.add_rotate_y(
                    dat, angle=-np.pi / 2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=4 * self.setup.t_cph_gate.item() +
                    self.setup.t_gate.item(),
                )
            for anc in self._x_anc_bits:
                p = self.setup.sel(qubit=anc)
                circuit.add_rotate_y(
                    anc, angle=np.pi / 2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=0,
                )
                circuit.add_rotate_y(
                    anc, angle=-np.pi / 2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=4 * self.setup.t_cph_gate.item() +
                    self.setup.t_gate.item(),
                )

            for anc in self._z_anc_bits:
                p = self.setup.sel(qubit=anc)
                circuit.add_rotate_y(
                    anc, angle=np.pi/2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=z_seq_start,
                )
                circuit.add_rotate_y(
                    anc, angle=-np.pi/2,
                    dephasing_angle=p.dephasing_angle.item(),
                    dephasing_axis=p.dephasing_axis.item(),
                    time=z_seq_start + 4 * self.setup.t_cph_gate.item() +
                    self.setup.t_gate.item()
                )

            sampler = BiasedSampler(
                readout_error=self.setup.readout_error.item(),
                rng=self._rng, alpha=1)

            x_meas_start = (
                1.5*self.setup.t_gate.item()) + (4*self.setup.t_cph_gate.item())

            for x_anc in self._x_anc_bits:
                circuit.add_measurement(
                    x_anc, output_bit="M"+x_anc,
                    time=x_meas_start + self.setup.t_meas.item(),
                    sampler=sampler)

            z_meas_start = z_seq_start + \
                (1.5*self.setup.t_gate.item()) + \
                (4*self.setup.t_cph_gate.item())

            for z_anc in self._z_anc_bits:
                circuit.add_measurement(
                    z_anc,
                    time=z_meas_start + self.setup.t_meas.item(),
                    sampler=sampler,
                    output_bit="M"+z_anc)

            # Check that circuit is sane
            assert x_meas_start+2*self._p('t_meas') == \
                   self._p('t_cycle') - 0.5*self._p('t_gate')

            # Block for adding increased dephasing during parking
            z_park_starts = [x_meas_start, x_meas_start+self._p('t_meas')]
            z_park_ends = [x_meas_start+self._p('t_meas'),
                           self._p('t_cycle') - 0.5*self._p('t_gate')]
            x_park_starts = [-0.5*self._p('t_gate'),
                             z_meas_start,
                             z_meas_start + self._p('t_meas')]
            x_park_ends = [0.5*self._p('t_gate') + self._p('t_cph_gate'),
                           z_meas_start + self._p('t_meas'),
                           self._p('t_cycle') - 0.5*self._p('t_gate')]

            for bits, t_starts, t_ends in (
                    (self._z_anc_bits, z_park_starts, z_park_ends),
                    (self._x_anc_bits, x_park_starts, x_park_ends)):
                for t_start, t_end in zip(t_starts, t_ends):
                    for bit in bits:
                        duration = t_end - t_start
                        circuit.add_gate('amp_ph_damping', bit, t_start,
                                         duration,
                                         t1=self._p('t1', bit),
                                         t2=self._p('t2_park', bit))

            if self._p('meas_basis') not in ('Z', 'X'):
                raise ValueError('meas_basis must be "Z" or "X"')
            if self._p('meas_basis') == 'X':
                for db in self._data_bits:
                    circuit.add_gate('rotate_y', db, angle=np.pi/2, time=0.01)
                    circuit.add_gate('rotate_y', db, angle=-np.pi/2,
                                     time=self._p('t_cycle')-0.01)

            circuit.add_waiting_gates(
                tmin=0, tmax=self.setup.t_cycle.item())
            circuit.order()
            self._circuit = circuit
        return self._circuit

    @property
    def operators(self):
        if self._operators is None:
            gen_basis = ptm.GeneralBasis(3)

            operators = []
            for gate in self.circuit.gates:
                if gate.is_measurement:
                    operators.append(
                        (gate.involved_qubits[-1:], "measure"))
                elif isinstance(gate, AmpPhDamp):
                    qubit = gate.involved_qubits[0]
                    idling_ptm = idle(gate.duration, gate.t1, gate.t2,
                                      self.qubit_anharm[qubit])
                    operators.append(
                        (gate.involved_qubits, idling_ptm))
                elif isinstance(gate, RotateY):
                    sin, cos = np.sin(gate.angle / 2), np.cos(gate.angle/2)
                    qubit = gate.involved_qubits[0]
                    kraus = np.array(
                        [[cos, -sin, 0], [sin, cos, 0], [0, 0, 1]])
                    operators.append(
                        (gate.involved_qubits, ptm.ConjunctionPTM(kraus)))
                elif isinstance(gate, CPhase):
                    stat_q, fluxed_q = gate.involved_qubits
                    operators.append(([stat_q, fluxed_q],
                                      self._get_cphase(stat_q, fluxed_q,
                                                       gen_basis)))
                elif isinstance(gate, FluxPulse):
                    bit = gate.involved_qubits[0]
                    operators.append((gate.involved_qubits,
                                      self._get_detuning(bit, gen_basis)))
                else:
                    print("gate not converted: ", gate)

            operators.append((self._data_bits, "getdiag"))
            self._operators = operators
        return self._operators

    def set_compiler(self, num_cycles, use_log_superpos=False):
        if self._compiler is None:
            if use_log_superpos:
                u_cnot33 = np.eye(9)
                u_cnot33[1*3+0, 1*3+0] = 0
                u_cnot33[1*3+1, 1*3+1] = 0
                u_cnot33[1*3+1, 1*3+0] = 1
                u_cnot33[1*3+0, 1*3+1] = 1
                u_cnot33 = u_cnot33.reshape((3, 3, 3, 3))
                cnot = ptm.TwoKrausPTM(u_cnot33)
                fully_mixed2 = ptm.ExplicitBasisPTM(
                    np.array([[1, 1, 0, 0, 0, 0, 0, 0, 0]]*9).T,
                    ptm.GeneralBasis(3))

                init_ops = [(("C",), fully_mixed2)]
                init_ops += [(("C", "D"+str(db)), cnot) for db in range(9)]
                compiler = ptm.TwoPTMCompiler(
                    init_ops + (self.operators * num_cycles))
            else:
                compiler = ptm.TwoPTMCompiler(self.operators * num_cycles)

            compiler.contract_to_two_ptms()
            compiler.make_block_ptms()

            compiler.initial_bases = {b: ptm.GeneralBasis(
                3).get_subbasis([0]) for b in compiler.bits}
            print("compiling ptms...")
            compiler.basis_choice()
            print("compiled to {} blocks".format(
                len(compiler.compiled_blocks)))
            self._compiler = compiler

    def run_batch(self, seed_list):
        """After the single-cycle circuit has been generated,
        this function applies it repeatedly, while recording the syndrome
        measureements.
        """
        if self._compiler is None:
            raise ValueError('Please comile and optimize a circuit first')

        outcomes = []
        qubits = list(sorted(self._compiler.bits))
        for seed in seed_list:
            # since we are running these over multiple days the rep is shifted
            self._reseed_rng(seed)
            cur_stabs = defaultdict(list)
            cur_diags = []

            density_mat = dm10g.DensityGeneral(
                bases=[self._compiler.initial_bases[qubit] for qubit in qubits])

            for block in self._compiler.compiled_blocks:
                if isinstance(block.op, ptm.TwoPTM) and len(block.bits) == 2:
                    bit0, bit1 = block.bits
                    basis_in0, basis_in1 = block.in_basis
                    basis_out0, basis_out1 = block.out_basis

                    if block.bitmap[bit0] == 0:
                        bit0, bit1 = bit1, bit0
                        basis_in0, basis_in1 = basis_in1, basis_in0
                        basis_out0, basis_out1 = basis_out1, basis_out0

                    ind0, ind1 = qubits.index(bit0), qubits.index(bit1)

                    if density_mat.bases[ind0] != basis_in0:
                        density_mat.apply_ptm(
                            ind0,
                            ptm.ProductPTM([]).get_matrix(
                                density_mat.bases[ind0], basis_in0),
                            new_basis=basis_in0)
                    if density_mat.bases[ind1] != basis_in1:
                        density_mat.apply_ptm(
                            ind1,
                            ptm.ProductPTM([]).get_matrix(
                                density_mat.bases[ind1], basis_in1),
                            new_basis=basis_in1)

                    assert density_mat.bases[ind0] == basis_in0
                    assert density_mat.bases[ind1] == basis_in1

                    density_mat.apply_two_ptm(
                        bit0=ind0, bit1=ind1, ptm=block.ptm,
                        new_basis=[basis_out0, basis_out1])

                elif block.op == "measure":
                    bit0, = block.bits
                    ind0 = qubits.index(bit0)
                    proj = density_mat.partial_trace(ind0)
                    proj /= sum(proj)
                    # noinspection PyArgumentList
                    state = int(sum(self._rng.rand() > np.cumsum(proj)))
                    cur_stabs[bit0].append(state)
                    density_mat.project_measurement(ind0, state=state)

                elif block.op == "getdiag":
                    density_mat.renormalize()
                    cur_diags.append(density_mat.get_diag())

            stabilizer_keys = sorted(list(cur_stabs.keys()))
            stabilizer_data = [cur_stabs[k] for k in stabilizer_keys]
            outcomes.append(
                xr.Dataset(
                    data_vars={
                        'syndrome': (['ancilla', 'round'], stabilizer_data),
                        'diagonal': (['round', 'full_basis_index'], cur_diags),
                    },
                    coords={
                        'ancilla': stabilizer_keys,
                        'seed': seed,
                    },
                )
            )
        assert len(outcomes) > 0
        return xr.concat(outcomes, dim='seed')
