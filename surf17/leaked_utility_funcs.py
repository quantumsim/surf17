import numpy as np
from six import string_types
from itertools import product


def get_hamming_dist(m, n):
    '''Quick little function to calculate the hamming element of two integers

    Arguments:
        m {int} -- The first index
        n {int} -- The second index

    Returns:
        int -- the calculated hamming element (0 or 1)
    '''

    diff = m ^ n
    dist = 0
    while diff:
        dist += diff & 1
        diff >>= 1
    return dist


def get_char_inds(string, char):
    return [i for i, ltr in enumerate(string) if ltr == char]


def get_smear_mat_element(elem_0, elem_1, data_len, readout_error):
    leaked_inds_0 = get_char_inds(elem_0, '2')
    leaked_inds_1 = get_char_inds(elem_1, '2')

    if leaked_inds_1:
        return 0

    if leaked_inds_0:
        for ind in leaked_inds_0:
            if elem_1[ind] != '1':
                return 0

        deleaked_elem_0 = int(
            ''.join([char for ind, char in enumerate(elem_0) if ind not in leaked_inds_0]), 2)
        deleaked_elem_1 = int(
            ''.join([char for ind, char in enumerate(elem_1) if ind not in leaked_inds_0]), 2)

        hamming_dist = get_hamming_dist(deleaked_elem_0, deleaked_elem_1)
        return (readout_error**hamming_dist)*((1 - readout_error)**(data_len - len(leaked_inds_0) - hamming_dist))

    hamming_dist = get_hamming_dist(int(elem_0, 2), int(elem_1, 2))
    return (readout_error**hamming_dist)*((1 - readout_error)**(data_len - hamming_dist))


def gen_smear_mat(readout_error):
    """Function to generate the smearing matrix used by the noisy readout during the final measurement. Note that the final measurement is extracted from the density matrix for a more complete information.

    Arguments:
        readout_error {float} -- The readout error of the measurement model in use.

        data_len {int} -- The number of data qubits as defined by the error correcting code.

    Returns:
        np.array -- The calculated smearing matrix
    """
    data_len = 9
    data_outcomes = [
        ''.join(state_list) for state_list in list(product(
            '01', '01', '01', '012', '012', '012', '01', '01', '01'))]
    if readout_error is not 0:
        return np.array([[
            get_smear_mat_element(elem_0, elem_1, data_len, readout_error)
            for elem_0 in data_outcomes] for elem_1 in data_outcomes])
    return np.eye(len(data_outcomes))


def load_smear_mat(smear_mat_input, data_len):
    if isinstance(smear_mat_input, np.ndarray):
        assert smear_mat_input.shape == (2**data_len, 2**data_len)
        return smear_mat_input
    if isinstance(smear_mat_input, string_types):
        try:
            return np.load(smear_mat_input)
        except OSError as error:
            raise
    else:
        raise RuntimeError


def load_proj_mat(proj_mat_input, data_len, stab_len):
    if isinstance(proj_mat_input, np.ndarray):
        assert proj_mat_input.shape == ((2**(stab_len + 1), 2**data_len))
        return proj_mat_input
    if isinstance(proj_mat_input, string_types):
        try:
            return np.load(proj_mat_input)
        except OSError as error:
            raise
    else:
        raise RuntimeError


def get_par_vec(data_outcomes):
    shift_len = 16
    temp_vec = data_outcomes
    while shift_len > 0:
        temp_vec = np.bitwise_xor(
            np.right_shift(temp_vec, shift_len), temp_vec)
        shift_len //= 2

    parity_vec = np.bitwise_and(temp_vec, 1)
    return parity_vec


def get_stab_vec(data_outcomes, stab_len, anc_con_dic):
    stab_vec = np.zeros(len(data_outcomes), dtype=int)

    for i, anc_con in enumerate(anc_con_dic):
        sorted_anc_con = np.sort(anc_con)

        temp_vec = data_outcomes
        init_pos = sorted_anc_con[0]
        for pos in sorted_anc_con[1:]:
            temp_vec = np.bitwise_xor(np.right_shift(
                data_outcomes, abs(pos - init_pos)), temp_vec)

        stab_outcome = np.bitwise_and(
            np.right_shift(temp_vec, init_pos), 1)
        stab_vec = np.bitwise_or(np.left_shift(
            stab_outcome, (stab_len - 1 - i)), stab_vec)

    return stab_vec


def get_par_stab_vec(data_outcomes, stab_len, anc_con_dic):

    parity_vec = get_par_vec(data_outcomes=data_outcomes)

    stab_vec = get_stab_vec(data_outcomes=data_outcomes,
                            stab_len=stab_len,
                            anc_con_dic=anc_con_dic)

    par_stab_vec = np.bitwise_or(np.left_shift(parity_vec, stab_len), stab_vec)
    return par_stab_vec


def gen_proj_mat():
    anc_con_dic = [[0, 3], [1, 2, 4, 5], [3, 4, 6, 7], [5, 8]]

    diag_elements = list(product('01', '01', '01', '012',
                                 '012', '012', '01', '01', '01'))

    stab_len = 4

    measured_data_outcomes = [int(''.join(diag_element).replace(
        '2', '1'), 2) for diag_element in diag_elements]

    par_stab_vec = get_par_stab_vec(data_outcomes=measured_data_outcomes,
                                    stab_len=stab_len,
                                    anc_con_dic=anc_con_dic)

    stab_outcomes = np.arange(2**(stab_len+1))

    proj_mat = np.array([(par_stab_vec == stab_outcome).astype(int)
                         for stab_outcome in stab_outcomes])
    return proj_mat


def gen_leakage_mat():
    diag_string = [2, 2, 2, 3, 3, 3, 2, 2, 2]
    total_elems = (3**3)*(2**6)
    leakage_mat = np.zeros([3, total_elems])
    for index in range(total_elems):
        temp = index
        decomp = []
        for j in range(9):
            decomp.append(temp % diag_string[j])
            temp = temp // diag_string[j]
        decomp = list(reversed(decomp))

        for j in range(3):
            if decomp[j+3] == 2:
                leakage_mat[j, index] = 1
    return leakage_mat


def gen_leakage_removal_mat():
    data_outcomes = [''.join(state_list)
                     for state_list in list(product('01', '01', '01', '012', '012', '012', '01', '01', '01'))]
    comp_outcomes = np.arange(2**9)
    leakage_removal_mat = np.array([[1 if bin(comp_outcome)[2:].zfill(9) == data_outcome else 0
                                     for data_outcome in data_outcomes] for comp_outcome in comp_outcomes])
    return leakage_removal_mat


def gen_comp_proj_mat():
    anc_con_dic = [[0, 3], [1, 2, 4, 5], [3, 4, 6, 7], [5, 8]]

    data_outcomes = np.arange(2 ** 9)
    stab_len = 4

    par_stab_vec = get_par_stab_vec(data_outcomes=data_outcomes,
                                    stab_len=stab_len,
                                    anc_con_dic=anc_con_dic)

    num_stab_outcomes = 2**(stab_len+1)
    stab_outcomes = np.arange(num_stab_outcomes)

    proj_mat = np.array([(par_stab_vec == stab_outcome).astype(int)
                         for stab_outcome in stab_outcomes])

    leakage_removal_mat = gen_leakage_mat()

    comp_proj_mat = proj_mat @ leakage_removal_mat
    return comp_proj_mat
