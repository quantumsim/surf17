from functools import lru_cache
import numpy as np
from scipy.linalg import expm
from quantumsim.ptm import TwoKrausPTM, ProductPTM, LindbladPLM, AdjunctionPLM, ExplicitBasisPTM, TwoPTMProduct
from quantumsim.ptm import GeneralBasis as gen_basis


@lru_cache(maxsize=64)
def damp_plm(t1):
    op_t1 = np.sqrt(1./t1)*np.array([[0, 1, 0], [0, 0, np.sqrt(2)], [0, 0, 0]])
    return LindbladPLM(op_t1)


@lru_cache(maxsize=64)
def deph_plms(t1, t2):
    t_phi = 1./(1./t2 - 0.5/t1)
    ops_t2 = [
        np.sqrt(2./t_phi)*np.array([[0, 0, 0], [0, 1, 0], [0, 0, 2]])
    ]

    return (LindbladPLM(op_t2) for op_t2 in ops_t2)


@lru_cache(maxsize=64)
def unitary_plm(anharmonicity):
    ham = np.array([
        [0., 0., 0.],
        [0., 0., 0.],
        [0., 0., -anharmonicity],
    ])
    return AdjunctionPLM(2*np.pi*ham)


@lru_cache(maxsize=64)
def idle(duration, t1, t2, anharmonicity=0.):
    lindblad_plms = [damp_plm(t1),
                     *deph_plms(t1, t2)]

    if not np.allclose(anharmonicity, 0.):
        lindblad_plms.append(unitary_plm(anharmonicity))

    lindblad_plm_ops = [lindblad_plm.get_matrix(gen_basis(3))
                        for lindblad_plm in lindblad_plms]

    plm_op = (np.sum(lindblad_plm_ops, axis=0) * duration)
    return ExplicitBasisPTM(expm(plm_op), gen_basis(3))


default_cphase_params = dict(
    phase_22=0,
    leakage_mobility_phase=0,
    leakage_phase=-np.pi/2,
    leakage_mobility=0,
    leakage_rate=0.,
    q0_t1=np.inf,
    q0_t2=np.inf,
    q1_t1=np.inf,
    q1_t2=np.inf,
    q1_t2_int=None,
    q0_anharmonicity=0,
    q1_anharmonicity=0,
    rise_time=2,
    int_time=28,
    phase_corr_time=12,
    phase_corr_error=0,
    leakage_mobility_rate=0,
    quasistatic_flux=0.,
    sensitivity=0.,
    phase_diff_02_12=np.pi,
    phase_diff_20_21=0.,
)


@lru_cache(maxsize=64)
def cphase(angle=np.pi, *, integrate_idling=False, **kwargs):
    """

    Parameters
    ----------
    angle : float
        Conditional phase of a CPhase gate, default is :math:`\\pi`.
    integrate_idling : bool
        Whether to return
    model : str
        Error model (currently only 'NetZero' is implemented).
    **kwargs
        Parameters for the error model.

    Returns
    -------
    Operation
        Resulting CPhase operation. First qubit is static (low-frequency)
        qubit,
    """
    def p(name):
        return kwargs.get(name, default_cphase_params[name])

    for param in kwargs.keys():
        if param not in default_cphase_params.keys():
            raise ValueError('Unknown model parameter: {}'.format(param))

    int_point_time = p('int_time') - (4 * p('rise_time'))
    if np.isfinite(p('q1_t2')) and np.isfinite(p('q1_t2_int')):
        rise_t2 = (p('q1_t2') + p('q1_t2_int')) / 2
    else:
        rise_t2 = np.inf

    int_time = p('int_time')
    leakage_rate = p('leakage_rate')
    qstatic_deviation = int_time * np.pi * \
                        p('sensitivity') * (p('quasistatic_flux') ** 2)
    qstatic_interf_leakage = (0.5 - (2 * leakage_rate)) * \
                             (1 - np.cos(1.5 * qstatic_deviation))
    phase_corr_error = p('phase_corr_error')

    rot_angle = angle + (1.5 * qstatic_deviation) + (2 * phase_corr_error)

    ideal_unitary = expm(1j * _ideal_generator(
        phase_10=phase_corr_error,
        phase_01=phase_corr_error + qstatic_deviation,
        phase_11=rot_angle,
        phase_02=rot_angle,
        phase_12=p(
            'phase_diff_02_12') - rot_angle,
        phase_20=0,
        phase_21=p(
            'phase_diff_20_21'),
        phase_22=p('phase_22')
    ))
    noisy_unitary = expm(1j * _exchange_generator(
        leakage=4 * leakage_rate + qstatic_interf_leakage,
        leakage_phase=p('leakage_phase'),
        leakage_mobility=p('leakage_mobility_rate'),
        leakage_mobility_phase=p('leakage_mobility_phase'),
    ))
    cz_unitary = ideal_unitary @ noisy_unitary
    cz_op = TwoKrausPTM(cz_unitary.reshape(3, 3, 3, 3))

    if integrate_idling:
        q0_t1 = p('q0_t1')
        q0_t2 = p('q0_t2')
        q0_anharmonicity = p('q0_anharmonicity')
        q1_t1 = p('q1_t1')
        q1_t2 = p('q1_t2')
        q1_t2_int = p('q1_t2_int')
        q1_anharmonicity = p('q1_anharmonicity')
        rise_time = p('rise_time')
        phase_corr_time = p('phase_corr_time')
        return TwoPTMProduct([
            ((0,), idle(int_time / 2, q0_t1, q0_t2, q0_anharmonicity)),
            ((1,), idle(rise_time, q1_t1, rise_t2, q1_anharmonicity)),
            ((1,), idle(int_point_time / 2, q1_t1, q1_t2_int,
                        q1_anharmonicity)),
            ((1,), idle(rise_time, q1_t1, rise_t2, q1_anharmonicity)),
            ((0, 1), cz_op),
            ((1,), idle(rise_time, q1_t1, rise_t2, q1_anharmonicity)),
            ((1,), idle(int_point_time / 2, q1_t1, q1_t2_int,
                        q1_anharmonicity)),
            ((1,), idle(rise_time, q1_t1, rise_t2, q1_anharmonicity)),
            ((0,), idle(int_time / 2, q0_t1, q0_t2, q0_anharmonicity)),
            ((0,), idle(phase_corr_time, q0_t1, q0_t2, q0_anharmonicity)),
            ((1,), idle(phase_corr_time, q1_t1, q1_t2, q1_anharmonicity))
        ])
    else:
        return cz_op


@lru_cache(maxsize=64)
def _ideal_generator(phase_01,
                     phase_02,
                     phase_10,
                     phase_11,
                     phase_12,
                     phase_20,
                     phase_21,
                     phase_22):
    phases = np.array([0, phase_01, phase_02, phase_10,
                       phase_11, phase_12, phase_20, phase_21, phase_22])
    generator = np.diag(phases).astype(complex)
    return generator


@lru_cache(maxsize=64)
def _exchange_generator(leakage, leakage_phase,
                        leakage_mobility, leakage_mobility_phase):
    generator = np.zeros((9, 9), dtype=complex)

    generator[2][4] = 1j * \
                      np.arcsin(np.sqrt(leakage)) * np.exp(1j * leakage_phase)
    generator[4][2] = -1j * \
                      np.arcsin(np.sqrt(leakage)) * np.exp(-1j * leakage_phase)

    generator[5][7] = 1j * np.arcsin(np.sqrt(leakage_mobility)) * \
                      np.exp(1j * leakage_mobility_phase)
    generator[7][5] = -1j * \
                      np.arcsin(np.sqrt(leakage_mobility)) * \
                      np.exp(-1j * leakage_mobility_phase)

    return generator


def det_pulse(t1, t2, t2_int, anharmonicity,
              rise_time=2, det_time=28, phase_corr_time=12):

    det_point_time = det_time - (4*rise_time)
    rise_t2 = (t2 + t2_int)/2
    det_ops = [
        idle(rise_time, t1, rise_t2, anharmonicity),
        idle(det_point_time / 2, t1, t2_int, anharmonicity),
        idle(2*rise_time, t1, rise_t2, anharmonicity),
        idle(det_point_time / 2, t1, t2_int, anharmonicity),
        idle(rise_time, t1, rise_t2, anharmonicity),
        idle(phase_corr_time, t1, t2, anharmonicity)
    ]

    return ProductPTM(det_ops)


def _kraus_unitary(kraus_ops, *, tbw_tol=1e-6):
    assert len(kraus_ops.shape) in (2, 3)
    dim_hilbert = kraus_ops.shape[1]
    op_products = np.sum([kraus.conj().T.dot(kraus)
                          for kraus in kraus_ops], axis=0)
    return np.sum(op_products)/dim_hilbert - 1 < tbw_tol
