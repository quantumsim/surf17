import pytest
import numpy as np
from surf17.experiment_setup import quick_setup
from surf17.qsim_circuit_builder import Surface17

data_bits = ["D%d" % i for i in range(3**2)]
x_anc_bits = ["X%d" % i for i in range(int((3**2-1)/2))]
z_anc_bits = ["Z%d" % i for i in range(int((3**2-1)/2))]
qubit_list = data_bits + x_anc_bits + z_anc_bits


class TestSurface17(object):
    def test_init(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        assert not surface17.get_measurements()
        assert not surface17.get_diagonals()

        assert len(surface17._data_bits) == 9
        assert len(surface17._x_anc_bits) == 4
        assert len(surface17._z_anc_bits) == 4
        assert len(surface17._meas_bits) == 8

        assert len(surface17.qubit_dic) == 17
        assert len(surface17.qubit_dic['D0']) == 5

        assert surface17._smeared_proj_matrix.shape == (32, 512)

        with pytest.raises(AssertionError) as err:
            surface17 = Surface17(seed=42, distance=0, setup=setup)

        with pytest.raises(AssertionError) as err:
            surface17 = Surface17(distance=3, setup=setup)

    def test_prep_circuit(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        circuit = surface17.prep_circuit()
        assert len(circuit.qubits) == 25
        assert not circuit.gates
        assert circuit.title == 'Default Circuit Name'

        circuit = surface17.prep_circuit(circuit_name='Test Name')
        assert circuit.title == 'Test Name'

    def test_build_cycle_circuit(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        surf17_circuit = surface17.build_cycle_circuit()

        assert len(surf17_circuit.qubits) == 25
        assert surf17_circuit.gates
        assert surf17_circuit.title == 'Surface Code Cycle'

    def test_make_state(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        state = surface17.make_state()

        assert state.no_qubits == 25
        for i in range(9):
            assert state.idx_in_full_dm['D%d' % i] == i
        assert state.max_bits_in_full_dm == 9
        assert len(state.classical) == 16

    def test_get_measurements(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        assert not surface17.get_measurements()

        surface17.run_code(cycles=1)
        stabs = surface17.get_measurements()
        assert stabs
        for stab in stabs:
            assert len(stab) == 8

    def test_get_diagonals(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        assert not surface17.get_diagonals()

        surface17.run_code(cycles=1)
        diagonals = surface17.get_diagonals()
        assert diagonals
        for diag in diagonals:
            assert len(diag) == 512
            assert round(sum(diag), 1) == 1

    def test_get_final_probs(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        assert not surface17.get_final_probs()

        surface17.run_code(cycles=1)
        final_probs = surface17.get_final_probs()
        assert final_probs
        for final_prob in final_probs:
            assert len(final_prob) == 32
            assert round(sum(final_prob), 1) == 1

    def test_run_code(self):
        setup = quick_setup(seed=42, qubit_list=qubit_list)
        surface17 = Surface17(seed=42, distance=3, setup=setup)
        surface17.run_code(cycles=1)

        assert len(surface17.get_measurements()) == 1
        assert len(surface17.get_diagonals()) == 1
