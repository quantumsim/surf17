import numpy as np
import surf17.utility_functions as utility


def test_get_hamming_dist():
    assert utility.get_hamming_dist(0, 0) == 0
    assert utility.get_hamming_dist(0, 1) == 1
    assert utility.get_hamming_dist(0, 15) == 4


def test_calc_smearing_matrix():
    clean_smear_mat = utility.gen_smear_mat(0, 9)
    assert clean_smear_mat.shape == (512, 512)
    assert np.all(clean_smear_mat == np.eye(2**9))

    smear_mat = utility.gen_smear_mat(0.0015, 9)
    assert smear_mat.shape == (512, 512)
    assert round(smear_mat[0][1], 4) == 0.0015
    assert round(smear_mat[1][0], 4) == 0.0015
    for meas_prob in smear_mat[0][1:]:
        assert smear_mat[0][0] > meas_prob
    assert round(smear_mat[0][0], 4) == round(smear_mat[511][511], 4)


def test_calc_z_proj_matrix():
    z_proj_mat = utility.gen_proj_mat(9, 4)
    assert z_proj_mat.shape == (32, 512)
    assert z_proj_mat[0][0] == 1
    assert z_proj_mat[24][1] == 1
    assert z_proj_mat[0][1] == 0
