import tempfile
import numpy as np
from surf17 import experiment_setup as exp_setup


class TestSetup(object):
    def test_init(self):
        setup = exp_setup.Setup()
        assert not setup.qubit_dic
        assert not setup.circuit_params

    def test_save_load(self):
        test_qubit_list = ['Q0', 'Q1']
        test_setup = exp_setup.quick_setup(seed=42, qubit_list=test_qubit_list)

        with tempfile.NamedTemporaryFile() as file:
            test_setup.save(filename=file.name)
            loaded_setup = exp_setup.Setup(filename=file.name)

        assert test_setup.qubit_dic == loaded_setup.qubit_dic
        assert test_setup.circuit_params == loaded_setup.circuit_params


def test_quick_setup():
    test_qubit_list = ['Q0']
    test_setup = exp_setup.quick_setup(seed=42, qubit_list=test_qubit_list)

    assert hasattr(test_setup, 'qubit_dic')
    assert hasattr(test_setup, 'circuit_params')

    assert 'Q0' in test_setup.qubit_dic.keys()
    assert len(test_setup.qubit_dic) == 1

    test_qubit_list = ['Q0', 'Q1']
    test_setup = exp_setup.quick_setup(seed=42, qubit_list=test_qubit_list)
    assert 'Q0' in test_setup.qubit_dic.keys()
    assert 'Q1' in test_setup.qubit_dic.keys()
    assert len(test_setup.qubit_dic) == 2


def get_qubit():
    rng = np.random.RandomState(42)
    test_qubit_dic = exp_setup.get_qubit(rng=rng)
    assert 't1' in test_qubit_dic.keys()
    assert 't2' in test_qubit_dic.keys()

    test_qubit_dic = exp_setup.get_qubit(rng=rng, noise_flag=False)
    assert 't1' in test_qubit_dic.keys()
    assert 't2' in test_qubit_dic.keys()
    assert 'dephasing_axis' in test_qubit_dic.keys()
    assert test_qubit_dic['t1'] == np.inf
    assert test_qubit_dic['t2'] == np.inf
    assert test_qubit_dic['dephasing_axis'] == 0

    test_qubit_dic = exp_setup.get_qubit(
        rng=rng, mean_t1=10, t1_fluc_flag=False)
    assert 't1' in test_qubit_dic.keys()
    assert test_qubit_dic['t1'] == 10

    test_qubit_dic = exp_setup.get_qubit(
        rng=rng, readout_error=0, rand_param=5)
    assert 'readout_error' not in test_qubit_dic.keys()
    assert 'rand_param' not in test_qubit_dic.keys()


def test_get_circuit_params():
    test_circut_params = exp_setup.get_circuit_params()
    assert len(test_circut_params) == 10

    assert 't_gate' in test_circut_params.keys()
    assert 't_cph_gate' in test_circut_params.keys()
    assert 'readout_error' in test_circut_params.keys()

    test_circut_params = exp_setup.get_circuit_params(t_gate=44)
    assert len(test_circut_params) == 10
    assert 't_gate' in test_circut_params.keys()
    assert test_circut_params['t_gate'] == 44

    test_circut_params = exp_setup.get_circuit_params(noise_flag=False)
    assert len(test_circut_params) == 10
    assert 't_gate' in test_circut_params.keys()
    assert 'readout_error' in test_circut_params.keys()
    assert 'p_exc_fin' in test_circut_params.keys()
    assert test_circut_params['t_gate'] == 20
    assert test_circut_params['readout_error'] == 0
    assert test_circut_params['p_exc_fin'] == 0

    test_circut_params = exp_setup.get_circuit_params(mean_t1=0, rand_param=5)
    assert 'mean_t1' not in test_circut_params.keys()
    assert 'rand_param' not in test_circut_params.keys()
