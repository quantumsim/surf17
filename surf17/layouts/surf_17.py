
class RotatedSurfaceCode:
    def __init__(self, distance=3):
        self._data_qubits = ["D%d" % i for i in range(distance**2)]
        self._x_stabs = ["X%d" %
                         i for i in range(int((distance**2 - 1) / 2))]
        self._z_stabs = ["Z%d" %
                         i for i in range(int((distance ** 2 - 1) / 2))]

        fg_purp = ["D0", "D2", "D6", "D8"]
        fg_purp_p = ["D1", "D7"]
        fg_red = ["D3", "D5"]
        fg_red_p = ["D4"]
        fg_blue = ["X1", "X3"]
        fg_blue_p = ["X0", "X2"]
        fg_green = ["Z2", "Z3"]
        fg_green_p = ["Z0", "Z1"]

        self._low_freq_bits = fg_purp + fg_purp_p
        self._high_freq_bits = fg_red + fg_red_p
        self._mid_freq_bits = fg_green + fg_green_p + fg_blue + fg_blue_p

        self.freq_group = {}
        self.qubit_anharm = {}
        for bit in self._high_freq_bits:
            self.freq_group[bit] = 2
        for bit in self._low_freq_bits:
            self.freq_group[bit] = 1
        for bit in self._mid_freq_bits:
            self.freq_group[bit] = 0

        self._resonators = [(anc, dat) if self.freq_group[anc] > self.freq_group[dat]
                            else (dat, anc) for anc, dats in
                            [("X0", ["D2", "D1"]),
                             ("X1", ["D1", "D0", "D4", "D3"]),
                             ("X2", ["D5", "D4", "D8", "D7"]),
                             ("X3", ["D7", "D6", ]),
                             ("Z0", ["D0", "D3", ]),
                             ("Z1", ["D2", "D5", "D1", "D4"]),
                             ("Z2", ["D4", "D7", "D3", "D6"]),
                             ("Z3", ["D5", "D8"])] for dat in dats]

        self._x_stab_sequences = [
            fg_red + fg_blue + fg_purp,
            fg_red_p + fg_blue + fg_purp_p,
            fg_red_p + fg_blue_p + fg_purp_p,
            fg_red + fg_blue_p + fg_purp]

        self._z_stab_sequences = [
            fg_green_p + fg_red_p + fg_purp_p,
            fg_green + fg_red + fg_purp,
            fg_green_p + fg_red + fg_purp,
            fg_green + fg_red_p + fg_purp_p,
        ]

    @property
    def x_stabs(self):
        return self._x_stabs

    @property
    def z_stabs(self):
        return self._z_stabs

    @property
    def data_qubits(self):
        return self._data_qubits

    @property
    def qubits(self):
        return self._data_qubits + self._x_stabs + self._z_stabs

    @property
    def stab_dance(self, dance_type):
        if dance_type == 'x':
            stab_sequences = self._x_stab_sequences
            stabs = self._x_stabs
        elif dance_type == 'z':
            stab_sequences = self._z_stab_sequences
            stabs = self._z_stabs
        else:
            raise ValueError(
                "Stabilizer dance must be either of the X or Z type")

        fluxed_pairs = []
        detuned_qubits = []
        for cur_sequence in stab_sequences:
            int_low_freq_qubits = []
            cur_fluxed_pairs = []
            for bit_0, bit_1 in self._resonators:
                if bit_0 in stabs or bit_1 in stabs:
                    if bit_0 in cur_sequence and bit_1 not in cur_sequence:
                        cur_fluxed_pairs.append((bit_1, bit_0))
                        if bit_1 in self._low_freq_bits:
                            int_low_freq_qubits.append(bit_1)
            fluxed_pairs.append(cur_fluxed_pairs)
            detuned_qubits.append(
                [bit for bit in self._low_freq_bits if bit not in int_low_freq_qubits])

        return fluxed_pairs, detuned_qubits
