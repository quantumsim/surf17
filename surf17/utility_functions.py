import numpy as np
from six import string_types
from itertools import product


def get_smear_mat_element(i, j, data_len, readout_error):
    hamming_dist = get_hamming_dist(i, j)
    return (readout_error**hamming_dist *
            (1 - readout_error)**(data_len - hamming_dist))


def get_hamming_dist(m, n):
    """Quick little function to calculate the hamming element of two integers

    Arguments:
        m {int} -- The first index
        n {int} -- The second index

    Returns:
        int -- the calculated hamming element (0 or 1)
    """

    diff = m ^ n
    dist = 0
    while diff:
        dist += diff & 1
        diff >>= 1
    return dist


def get_par_vec(data_len):
    num_data_outcomes = 2**data_len
    data_outcomes = np.arange(num_data_outcomes)

    shift_len = 16
    temp_vec = data_outcomes
    while shift_len > 0:
        temp_vec = np.bitwise_xor(
            np.right_shift(temp_vec, shift_len), temp_vec)
        shift_len //= 2

    parity_vec = np.bitwise_and(temp_vec, 1)
    return parity_vec


def get_stab_vec(data_len, stab_len, anc_con_dic):
    num_data_outcomes = 2**data_len
    data_outcomes = np.arange(num_data_outcomes)
    stab_vec = np.zeros(num_data_outcomes, dtype=int)

    for i, anc_con in enumerate(anc_con_dic):
        sorted_anc_con = np.sort(anc_con)

        temp_vec = data_outcomes
        init_pos = sorted_anc_con[0]
        for pos in sorted_anc_con[1:]:
            temp_vec = np.bitwise_xor(np.right_shift(
                data_outcomes, abs(pos - init_pos)), temp_vec)

        stab_outcome = np.bitwise_and(
            np.right_shift(temp_vec, init_pos), 1)
        stab_vec = np.bitwise_or(np.left_shift(
            stab_outcome, (stab_len - 1 - i)), stab_vec)

    return stab_vec


def get_par_stab_vec(data_len, stab_len, anc_con_dic):

    parity_vec = get_par_vec(data_len=data_len)

    stab_vec = get_stab_vec(data_len=data_len,
                            stab_len=stab_len,
                            anc_con_dic=anc_con_dic)

    par_stab_vec = np.bitwise_or(np.left_shift(parity_vec, stab_len), stab_vec)
    return par_stab_vec


def gen_smear_mat(readout_error, data_len):
    """Function to generate the smearing matrix used by the noisy readout
    during the final measurement. Note that the final measurement is extracted
    from the density matrix for a more complete information.

    Arguments:
        readout_error {float} -- The readout error of the measurement model in
        use.

        data_len {int} -- The number of data qubits as defined by the
        error-correcting code.

    Returns:
        np.array -- The calculated smearing matrix
    """
    if readout_error is not 0:
        num_stab_outcomes = 2**data_len
        return np.array([[get_smear_mat_element(row_ind, col_ind, data_len, readout_error) for col_ind in range(
            num_stab_outcomes)] for row_ind in range(num_stab_outcomes)])
    else:
        return np.eye(2 ** data_len)


def gen_smear_mat_leaked(readout_error, data_outcomes):
    '''Function to generate the smearing matrix used by the noisy readout during the final measurement. Note that the final measurement is extracted from the density matrix for a more complete information.

    Arguments:
        readout_error {float} -- The readout error of the measurement model in use.

        data_len {int} -- The number of data qubits as defined by the error correcting code.

    Returns:
        np.array -- The calculated smearing matrix
    '''
    if readout_error is not 0:
        return None
    else:
        return np.eye(len(data_outcomes))


def gen_proj_mat(data_len, stab_len):
    anc_con_dic = [[0, 3], [1, 2, 4, 5], [3, 4, 6, 7], [5, 8]]

    par_stab_vec = get_par_stab_vec(data_len=data_len,
                                    stab_len=stab_len,
                                    anc_con_dic=anc_con_dic)

    num_stab_outcomes = 2**(stab_len+1)
    stab_outcomes = np.arange(num_stab_outcomes)

    porj_mat = np.array([(par_stab_vec == stab_outcome).astype(int)
                         for stab_outcome in stab_outcomes])
    return porj_mat


def load_smear_mat(smear_mat_input, data_len):
    if isinstance(smear_mat_input, np.ndarray):
        assert smear_mat_input.shape == (2**data_len, 2**data_len)
        return smear_mat_input
    elif isinstance(smear_mat_input, string_types):
        try:
            return np.load(smear_mat_input)
        except OSError as error:
            raise
    else:
        raise RuntimeError


def load_proj_mat(proj_mat_input, data_len, stab_len):
    if isinstance(proj_mat_input, np.ndarray):
        assert proj_mat_input.shape == (2 ** (stab_len + 1), 2 ** data_len)
        return proj_mat_input
    elif isinstance(proj_mat_input, string_types):
        try:
            return np.load(proj_mat_input)
        except OSError as error:
            raise
    else:
        raise RuntimeError


def get_par_vec_leaked(data_outcomes):
    shift_len = 16
    temp_vec = data_outcomes
    while shift_len > 0:
        temp_vec = np.bitwise_xor(
            np.right_shift(temp_vec, shift_len), temp_vec)
        shift_len //= 2

    parity_vec = np.bitwise_and(temp_vec, 1)
    return parity_vec


def get_stab_vec_leaked(data_outcomes, stab_len, anc_con_dic):
    stab_vec = np.zeros(len(data_outcomes), dtype=int)

    for i, anc_con in enumerate(anc_con_dic):
        sorted_anc_con = np.sort(anc_con)

        temp_vec = data_outcomes
        init_pos = sorted_anc_con[0]
        for pos in sorted_anc_con[1:]:
            temp_vec = np.bitwise_xor(np.right_shift(
                data_outcomes, abs(pos - init_pos)), temp_vec)

        stab_outcome = np.bitwise_and(
            np.right_shift(temp_vec, init_pos), 1)
        stab_vec = np.bitwise_or(np.left_shift(
            stab_outcome, (stab_len - 1 - i)), stab_vec)

    return stab_vec


def get_par_stab_vec_leaked(data_outcomes, stab_len, anc_con_dic):

    parity_vec = get_par_vec_leaked(data_outcomes=data_outcomes)

    stab_vec = get_stab_vec_leaked(data_outcomes=data_outcomes,
                                   stab_len=stab_len,
                                   anc_con_dic=anc_con_dic)

    par_stab_vec = np.bitwise_or(np.left_shift(parity_vec, stab_len), stab_vec)
    return par_stab_vec


def gen_proj_mat_leaked():
    anc_con_dic = [[0, 3], [1, 2, 4, 5], [3, 4, 6, 7], [5, 8]]

    diag_elements = list(product('01', '01', '01', '012',
                                 '012', '012', '01', '01', '01'))

    stab_len = 4

    measured_data_outcomes = [int(''.join(diag_element).replace(
        '2', '1'), 2) for diag_element in diag_elements]

    par_stab_vec = get_par_stab_vec_leaked(data_outcomes=measured_data_outcomes,
                                           stab_len=stab_len,
                                           anc_con_dic=anc_con_dic)

    num_stab_outcomes = 2**(stab_len+1)
    stab_outcomes = np.arange(num_stab_outcomes)

    porj_mat = np.array([(par_stab_vec == stab_outcome).astype(int)
                         for stab_outcome in stab_outcomes])
    return porj_mat
