#!/usr/bin/env python3
##comment out lines by adding at least two `#' at the beginning
#SBATCH --job-name=s17_leakFT_incoh
#SBATCH --account=beenakker
#SBATCH --mail-type=END,FAIL
#SBATCH --partition=gpu
#SBATCH --gres=gpu:0
#SBATCH --output=s17_%j.out
#SBATCH --error=s17_%j.err
#SBATCH --mem=15000
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --ntasks-per-node=1

from surf17 import controller
from surf17.experiment_setup import get_qubit, get_circuit_params
from toolz import merge
import numpy as np
import xarray as xr


# rng not used for the setup (no t1-flux or quasistatic flux)
rng = np.random.RandomState(0)
t1 = 30000.
t2_sweetspot = 30000.
t2_park_lf = t2_sweetspot
t2_park_mf = t2_sweetspot

qubit_dic = merge({
    # Low-frequency qubits
    q: get_qubit(rng=rng,
                 t1=t1,
                 t2=t2_sweetspot,
                 t2_park=t2_park_lf,
                 qstatic_flux=0.,
                 )
    for q in ['D0', 'D1', 'D2', 'D6', 'D7', 'D8']
}, {
    # Mid-frequency qubits
    q: get_qubit(rng=rng,
                 t1=t1,
                 # Hack: most of the time ancillas spend in the
                 # parking position, so default t2 is parking one;
                 #
                 t2=t2_sweetspot,
                 t2_park=t2_park_mf,
                 qstatic_flux=0.,
                 )
    for q in ['X0', 'X1', 'X2', 'X3', 'Z0', 'Z1', 'Z2', 'Z3']
}, {
    # High-frequency qubits
    q: get_qubit(rng=rng,
                 t1=t1,
                 t2=t2_sweetspot,
                 t2_park=np.nan,
                 qstatic_flux=0.,
                 )
    for q in ['D3', 'D4', 'D5']
})

qubit_list = list(qubit_dic.keys())
circuit_params = get_circuit_params()
qubit_params = list(qubit_dic[qubit_list[0]].keys())

cphase_params = dict(
    cphase_model='precomputed',
    cphase_leakage_model='incoherent',
    cphase_mid_high='qsim_qm_qh_int_30_corr_10_full_cz_ptm.npy',
    cphase_low_mid='qsim_ql_qm_int_30_corr_10_full_cz_ptm_reduced.npy'
)

qubit_setup = xr.Dataset(
    coords=merge(
        {'qubit': qubit_list},
        {param: ('qubit', [qubit_dic[qubit][param] for qubit in qubit_list])
         for param in qubit_params},
        circuit_params,
        cphase_params,
    ))

controller.run(qubit_setup)
