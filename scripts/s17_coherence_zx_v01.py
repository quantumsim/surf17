#!/usr/bin/env python3
##comment out lines by adding at least two `#' at the beginning
#SBATCH --job-name=s17_coherence_zs
#SBATCH --account=beenakker
#SBATCH --mail-type=END,FAIL
#SBATCH --partition=gpu
#SBATCH --gres=gpu:0
#SBATCH --output=s17_%j.out
#SBATCH --error=s17_%j.err
#SBATCH --mem=15000
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --ntasks-per-node=1

import numpy as np
import xarray as xr

from itertools import product
from toolz import merge
from surf17 import controller
from surf17.experiment_setup import get_qubit, get_circuit_params

rng = np.random.RandomState(0)
t1 = 30000.
t2_sweetspot = 30000.
t2_int_data = 30000.
t2_int_anc = 30000.
t2_park_lf = 30000.
t2_park_mf = 30000.

qubit_setups = []

for cphase_leakage_model, basis in product(
        ('incoherent',), ('Z', 'X')
):
    qubit_dic = merge({
        # Low-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     t2=t2_sweetspot,
                     t2_int=np.nan,
                     t2_park=t2_park_lf,
                     cphase_leakage_rate=0.,
                     cphase_leakage_mobility_rate=0.,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi,
                     cphase_phase_diff_20_21=0.,
                     cphase_leakage_phase=-0.5*np.pi,
                     ) for q in ['D0', 'D1', 'D2', 'D6', 'D7', 'D8']
    }, {
        # Mid-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     # Hack: most of the time ancillas spend in the
                     # parking position, so default t2 is parking one;
                     #
                     t2=t2_sweetspot,
                     t2_int=t2_int_anc,
                     t2_park=t2_park_mf,
                     cphase_leakage_rate=0.005,
                     cphase_leakage_mobility_rate=0.,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi,
                     cphase_phase_diff_20_21=0.,
                     cphase_leakage_phase=-0.5*np.pi,
                     ) for q in ['X0', 'X1', 'X2', 'X3',
                                 'Z0', 'Z1', 'Z2', 'Z3']
    }, {
        # High-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     t2=t2_sweetspot,
                     t2_int=t2_int_data,
                     t2_park=np.nan,
                     cphase_leakage_rate=0.005,
                     cphase_leakage_mobility_rate=0.,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi,
                     cphase_phase_diff_20_21=0.,
                     cphase_leakage_phase=-0.5*np.pi,
                     ) for q in ['D3', 'D4', 'D5']
    }
    )
    qubit_list = list(qubit_dic.keys())
    circuit_params = get_circuit_params(meas_basis=basis)
    qubit_params = list(qubit_dic[qubit_list[0]].keys())

    cphase_params = dict(
        cphase_model='blackbox',
        cphase_rise_time=2.,
        cphase_int_time=30.,
        cphase_phase_corr_time=10.,
        cphase_angle=np.pi,
        cphase_phase_corr_error=0.,
        cphase_leakage_model=cphase_leakage_model,
        cphase_integrate_idling=True,
    )

    qubit_setups.append((
        "basis{}_{}".format(basis, cphase_leakage_model),
        xr.Dataset(
            coords=merge(
                {'qubit': qubit_list},
                {param: (
                'qubit', [qubit_dic[qubit][param] for qubit in qubit_list])
                 for param in qubit_params},
                circuit_params,
                cphase_params,
            ))))

for suffix, qubit_setup in qubit_setups:
    controller.run(qubit_setup, use_log_superpos=True, name_suffix=suffix)
