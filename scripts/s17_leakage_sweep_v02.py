#!/usr/bin/env python3
##comment out lines by adding at least two `#' at the beginning
#SBATCH --job-name=s17_lr_sweep_train
#SBATCH --account=beenakker
#SBATCH --mail-type=END,FAIL
#SBATCH --partition=gpu
#SBATCH --error=s17_%j.err
#SBATCH --mem=10000
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks-per-node=1

import os
task_id = os.getenv('SLURM_ARRAY_TASK_ID')
if task_id is not None:
    task_id = int(task_id)
    task_min = int(os.getenv('SLURM_ARRAY_TASK_MIN'))
    task_max = int(os.getenv('SLURM_ARRAY_TASK_MAX'))
    ntasks = task_max - task_min + 1
    gpu = 0 if task_id < task_min + ntasks // 2 else 1
    os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu)
else:
    task_min = 1
    task_max = 1
    task_id = 1

import numpy as np
import xarray as xr

from toolz import merge
from surf17 import controller
from surf17.experiment_setup import get_qubit, get_circuit_params

rng = np.random.RandomState(0)

t1 = 30000.
t2_sweetspot = 30000.
t2_int_data = 7000.
t2_int_anc = 5500.
t2_park_lf = 8000.
t2_park_mf = 7300.

qubit_setups = []

cphase_leakage_model = 'incoherent'
anharmonicity = False
leakage_phase = -np.pi/2
leakage_mobility = 0.
# phase_diff_12_21 = 0.
basis = 'Z'

for leakage_rate in np.array([0.00025, 0.00125, 0.0025, 0.00375, 0.005]):
    qubit_dic = merge({
        # Low-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     t2=t2_sweetspot,
                     t2_int=np.nan,
                     t2_park=t2_park_lf,
                     anharmonicity=306. if anharmonicity else 0.,
                     cphase_leakage_rate=np.nan,
                     cphase_leakage_mobility_rate=np.nan,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=rng.random_sample() * np.pi,
                     cphase_phase_diff_20_21=rng.random_sample() * np.pi,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['D0', 'D1', 'D2', 'D6', 'D7', 'D8']
    }, {
        # Mid-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     t2=t2_sweetspot,
                     t2_int=t2_int_anc,
                     t2_park=t2_park_mf,
                     anharmonicity=308. if anharmonicity else 0.,
                     cphase_leakage_rate=leakage_rate,
                     cphase_leakage_mobility_rate=0.,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=rng.random_sample() * np.pi,
                     cphase_phase_diff_20_21=rng.random_sample() * np.pi,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['X0', 'X1', 'X2', 'X3',
                                 'Z0', 'Z1', 'Z2', 'Z3']
    }, {
        # High-frequency qubits
        q: get_qubit(rng=rng,
                     t1=t1,
                     t2=t2_sweetspot,
                     t2_int=t2_int_data,
                     t2_park=np.nan,
                     anharmonicity=331. if anharmonicity else 0.,
                     cphase_leakage_rate=leakage_rate,
                     cphase_leakage_mobility_rate=leakage_mobility,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=rng.random_sample() * np.pi,
                     cphase_phase_diff_20_21=rng.random_sample() * np.pi,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['D3', 'D4', 'D5']
    }
    )
    qubit_list = list(qubit_dic.keys())
    circuit_params = get_circuit_params(meas_basis=basis)
    qubit_params = list(qubit_dic[qubit_list[0]].keys())

    cphase_params = dict(
        cphase_model='blackbox',
        cphase_rise_time=2.,
        cphase_int_time=30.,
        cphase_phase_corr_time=10.,
        cphase_angle=np.pi,
        cphase_phase_corr_error=0.,
        cphase_leakage_model=cphase_leakage_model,
        cphase_integrate_idling=True,
    )

    qubit_setups.append((
        "basis{}_lr{:.5f}"
        .format(basis, leakage_rate),
        xr.Dataset(
            coords=merge(
                {'qubit': qubit_list},
                {param: (
                    'qubit', [qubit_dic[qubit][param] for qubit in qubit_list])
                    for param in qubit_params},
                circuit_params,
                cphase_params,
            ))))

for suffix, qubit_setup in qubit_setups:
    controller.run(qubit_setup, use_log_superpos=True, name_suffix=suffix, )
