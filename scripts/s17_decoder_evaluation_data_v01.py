#!/usr/bin/env python3
##comment out lines by adding at least two `#' at the beginning
#SBATCH --job-name=s17_detuning
#SBATCH --account=beenakker
#SBATCH --mail-type=END,FAIL
#SBATCH --partition=gpu
#SBATCH --error=s17_%j.err
#SBATCH --mem=10000
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks-per-node=1

import os
task_id = os.getenv('SLURM_ARRAY_TASK_ID')
if task_id is not None:
    task_id = int(task_id)
    task_min = int(os.getenv('SLURM_ARRAY_TASK_MIN'))
    task_max = int(os.getenv('SLURM_ARRAY_TASK_MAX'))
    ntasks = task_max - task_min + 1
    gpu = 0 if task_id < task_min + ntasks // 2 else 1
    os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu)
else:
    task_min = 1
    task_max = 1
    task_id = 1

import numpy as np
import xarray as xr

from toolz import merge
from surf17 import controller
from surf17.experiment_setup import get_qubit, get_circuit_params

rng = np.random.RandomState(0)

qubit_setups = []

cphase_leakage_model = 'incoherent'
anharmonicity = True
leakage_phase = -np.pi/2
leakage_mobility = 0.
# phase_diff_12_21 = 0.
basis = 'Z'

for phase_diff_12_21 in [0.]:
    qubit_dic = merge({
        # Low-frequency qubits
        q: get_qubit(rng=rng,
                     t1=25e3,
                     t2=30e3,
                     t2_int=np.nan,
                     t2_park=3.5e3,
                     anharmonicity=306. if anharmonicity else 0.,
                     cphase_leakage_rate=np.nan,
                     cphase_leakage_mobility_rate=np.nan,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi - phase_diff_12_21,
                     cphase_phase_diff_20_21=phase_diff_12_21,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['D0', 'D1', 'D2', 'D6', 'D7', 'D8']
    }, {
        # Mid-frequency qubits
        q: get_qubit(rng=rng,
                     t1=17e3,
                     # Hack: most of the time ancillas spend in the
                     # parking position, so default t2 is parking one;
                     #
                     t2=22e3,
                     t2_int=2.5e3,
                     t2_park=2.5e3,
                     anharmonicity=308. if anharmonicity else 0.,
                     cphase_leakage_rate=0.0064*2,
                     cphase_leakage_mobility_rate=0.,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi - phase_diff_12_21,
                     cphase_phase_diff_20_21=phase_diff_12_21,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['X0', 'X1', 'X2', 'X3',
                                 'Z0', 'Z1', 'Z2', 'Z3']
    }, {
        # High-frequency qubits
        q: get_qubit(rng=rng,
                     t1=26e3,
                     t2=15e3,
                     t2_int=2e3,
                     t2_park=np.nan,
                     anharmonicity=331. if anharmonicity else 0.,
                     cphase_leakage_rate=(0.004 + 0.002)*2,
                     cphase_leakage_mobility_rate=leakage_mobility,
                     qstatic_flux=0.,
                     cphase_sensitivity=40.,
                     cphase_phase_diff_02_12=np.pi - phase_diff_12_21,
                     cphase_phase_diff_20_21=phase_diff_12_21,
                     cphase_leakage_phase=leakage_phase,
                     ) for q in ['D3', 'D4', 'D5']
    }
    )
    qubit_list = list(qubit_dic.keys())
    circuit_params = get_circuit_params(meas_basis=basis)
    qubit_params = list(qubit_dic[qubit_list[0]].keys())

    cphase_params = dict(
        cphase_model='blackbox',
        cphase_rise_time=2.,
        cphase_int_time=30.,
        cphase_phase_corr_time=10.,
        cphase_angle=np.pi,
        cphase_phase_corr_error=0.,
        cphase_leakage_model=cphase_leakage_model,
        cphase_integrate_idling=True,
    )

    qubit_setups.append((
        "basis{}_1221phase{:.3f}"
        .format(basis, phase_diff_12_21),
        xr.Dataset(
            coords=merge(
                {'qubit': qubit_list},
                {param: (
                    'qubit', [qubit_dic[qubit][param] for qubit in qubit_list])
                    for param in qubit_params},
                circuit_params,
                cphase_params,
            ))))

for suffix, qubit_setup in qubit_setups:
    controller.run(qubit_setup, use_log_superpos=True, name_suffix=suffix, )
