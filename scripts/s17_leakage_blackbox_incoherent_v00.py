#!/usr/bin/env python3
##comment out lines by adding at least two `#' at the beginning
#SBATCH --job-name=s17_leakage_blackbox
#SBATCH --account=beenakker
#SBATCH --mail-type=END,FAIL
#SBATCH --partition=gpu
#SBATCH --gres=gpu:0
#SBATCH --output=s17_%j.out
#SBATCH --error=s17_%j.err
#SBATCH --mem=15000
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --ntasks-per-node=1

from surf17 import controller, quick_setup

# rng not used for the setup (no t1-flux or quasistatic flux)
qubit_setup = quick_setup(seed=4200000000,
                          static_flux_std=0,
                          cphase_model='blackbox',
                          cphase_leakage_model='incoherent',
                          )

controller.run(qubit_setup)
